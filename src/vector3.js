class Vector3 {
  constructor(x=0, y=0, z=0) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  get vector() {
    return [this.x, this.y, this.z];
  }

  add(...vectors) {
    for(const vector of vectors) {
      this.x += vector[0];
      this.y += vector[1];
      this.z += vector[2];
    }
    return [this.x, this.y, this.z];
  }

  multiply(...vectors) {
    for(const vector of vectors) {
      this.x *= vector[0];
      this.y *= vector[1];
      this.z *= vector[2];
    }
    return this;
  }

  negate() {
    this.x = -this.x;
    this.y = -this.y;
    this.z = -this.z;
    return [this.x, this.y, this.z];
  }

  scale(scale) {
    if(Array.isArray(scale))
      return this.multiply(scale);
    this.x *= scale;
    this.y *= scale;
    this.z *= scale;
    return this;
  }
}

module.exports = Vector3;
