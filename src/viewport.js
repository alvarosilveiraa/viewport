const {
  equals,
  createMat4,
  worldToPixels,
  pixelsToWorld,
  zoomToScale,
  getWorldPosition,
  lngLatToWorld,
  worldToLngLat,
  getProjectionMatrix,
  getViewMatrix,
  getKmZoom
} = require('./util');
const mat4_scale = require('gl-mat4/scale');
const mat4_translate = require('gl-mat4/translate');
const mat4_multiply = require('gl-mat4/multiply');
const mat4_invert = require('gl-mat4/invert');
const vec2_add = require('gl-vec2/add');
const vec2_negate = require('gl-vec2/negate');

class Viewport {
  constructor({
    width=1,
    height=1,
    latitude=0,
    longitude=0,
    km=0,
    zoom=0,
    pitch=0,
    bearing=0,
    altitude=1.5,
    farZMultiplier=10
  }) {
    this.width = width;
    this.height = height;
    this.scale = 1;

    zoom = km? getKmZoom(km): zoom;
    const scale = zoomToScale(zoom);
    altitude = Math.max(0.75, altitude);
    const center = getWorldPosition({
      longitude,
      latitude,
      scale
    });
    const projectionMatrix = getProjectionMatrix({
      width,
      height,
      pitch,
      bearing,
      altitude,
      farZMultiplier
    });
    const viewMatrix = getViewMatrix({
      height,
      center,
      pitch,
      bearing,
      altitude,
      flipY: true
    });

    this.viewMatrix = viewMatrix;
    this.projectionMatrix = projectionMatrix;
    const vpm = createMat4();
    mat4_multiply(vpm, vpm, this.projectionMatrix);
    mat4_multiply(vpm, vpm, this.viewMatrix);
    this.viewProjectionMatrix = vpm;
    const m = createMat4();
    mat4_scale(m, m, [this.width / 2, -this.height / 2, 1]);
    mat4_translate(m, m, [1, -1, 0]);
    mat4_multiply(m, m, this.viewProjectionMatrix);
    const mInverse = mat4_invert(createMat4(), m);
    if(!mInverse) throw new Error('Pixel project matrix not invertible');
    this.pixelProjectionMatrix = m;
    this.pixelUnprojectionMatrix = mInverse;

    this.latitude = latitude;
    this.longitude = longitude;
    this.zoom = zoom;
    this.pitch = pitch;
    this.bearing = bearing;
    this.altitude = altitude;
    this.scale = scale;
    this.center = center;
    Object.freeze(this);
  }

  equals(viewport) {
    if(!(viewport instanceof Viewport)) return false;
    return viewport.width === this.width &&
      viewport.height === this.height &&
      equals(viewport.projectionMatrix, this.projectionMatrix) &&
      equals(viewport.viewMatrix, this.viewMatrix);
  }

  project(xyz, topLeft=true) {
    const [x0, y0, z0] = xyz;
    const [X, Y] = this.projectFlat([x0, y0]);
    const coord = worldToPixels([X, Y, z0], this.pixelProjectionMatrix);
    const [x, y] = coord;
    const y2 = topLeft? y: this.height - y;
    return xyz.length === 2? [x, y2]: [x, y2, coord[2]];
  }

  unproject(xyz, topLeft=true, targetZ) {
    const [x, y, z] = xyz;
    const y2 = topLeft? y: this.height - y;
    const coord = pixelsToWorld([x, y2, z], this.pixelUnprojectionMatrix, targetZ);
    const [X, Y] = this.unprojectFlat(coord);
    if(Number.isFinite(z)) return [X, Y, coord[2]];
    return Number.isFinite(targetZ)? [X, Y, targetZ]: [X, Y];
  }

  projectFlat(lngLat, scale=this.scale) {
    return lngLatToWorld(lngLat, scale);
  }

  unprojectFlat(xy, scale=this.scale) {
    return worldToLngLat(xy, scale);
  }

  getLocationAtPoint({
    lngLat,
    pos
  }) {
    const fromLocation = pixelsToWorld(pos, this.pixelUnprojectionMatrix);
    const toLocation = lngLatToWorld(lngLat, this.scale);
    const translate = vec2_add([], toLocation, vec2_negate([], fromLocation));
    const newCenter = vec2_add([], this.center, translate);
    return worldToLngLat(newCenter, this.scale);
  }
}

module.exports = Viewport;