const Vector3 = require('./vector3');
const mat4_perspective = require('gl-mat4/perspective');
const mat4_translate = require('gl-mat4/translate');
const mat4_scale = require('gl-mat4/scale');
const mat4_rotateX = require('gl-mat4/rotateX');
const mat4_rotateZ = require('gl-mat4/rotateZ');
const vec2_lerp = require('gl-vec2/lerp');
const vec4_scale = require('gl-vec4/scale');
const vec4_transformMat4 = require('gl-vec4/transformMat4');

const PI = Math.PI;
const PI_4 = PI / 4;
const DEGREES_TO_RADIANS = PI / 180;
const RADIANS_TO_DEGREES = 180 / PI;
const TILE_SIZE = 512;
const EARTH_CIRCUMFERENCE = 40.03e6;
const DEFAULT_ALTITUDE = 1.5;

function assert(condition) {
  if(!condition) throw new Error('assertion failed.');
}

function equals(a, b) {
  if(isArray(a) && isArray(b)) {
    if(a === b) return true;
    if(a.length !== b.length) return false;
    for(let i = 0; i < a.length; ++i) {
      if(!equals(a[i], b[i])) return false;
    }
    return true;
  }
  return Math.abs(a - b) <= config.EPSILON * Math.max(1.0, Math.abs(a), Math.abs(b));
}

function createMat4() {
  return [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];
}

function transformVector(matrix, vector) {
  const result = vec4_transformMat4([], vector, matrix);
  vec4_scale(result, result, 1 / result[3]);
  return result;
}

function mod(value, divisor) {
  const modulus = value % divisor;
  return modulus < 0 ? divisor + modulus : modulus;
}

function lerp(start, end, step) {
  return step * end + (1 - step) * start;
}

function zoomToScale(zoom) {
  return Math.pow(2, zoom);
}

function scaleToZoom(scale) {
  return Math.log2(scale);
}

function lngLatToWorld([lng, lat], scale) {
  scale *= TILE_SIZE;
  const lambda2 = lng * DEGREES_TO_RADIANS;
  const phi2 = lat * DEGREES_TO_RADIANS;
  const x = scale * (lambda2 + PI) / (2 * PI);
  const y = scale * (PI - Math.log(Math.tan(PI_4 + phi2 * 0.5))) / (2 * PI);
  return [x, y];
}

function worldToLngLat([x, y], scale) {
  scale *= TILE_SIZE;
  const lambda2 = (x / scale) * (2 * PI) - PI;
  const phi2 = 2 * (Math.atan(Math.exp(PI - (y / scale) * (2 * PI))) - PI_4);
  return [lambda2 * RADIANS_TO_DEGREES, phi2 * RADIANS_TO_DEGREES];
}

function getMeterZoom({latitude}) {
  assert(Number.isFinite(latitude));
  const latCosine = Math.cos(latitude * DEGREES_TO_RADIANS);
  return scaleToZoom(EARTH_CIRCUMFERENCE * latCosine) - 9;
}

function getDistanceScales({latitude, longitude, zoom, scale, highPrecision = false}) {
  scale = scale !== undefined ? scale : zoomToScale(zoom);
  assert(Number.isFinite(latitude) && Number.isFinite(longitude) && Number.isFinite(scale));
  const result = {};
  const worldSize = TILE_SIZE * scale;
  const latCosine = Math.cos(latitude * DEGREES_TO_RADIANS);
  const pixelsPerDegreeX = worldSize / 360;
  const pixelsPerDegreeY = pixelsPerDegreeX / latCosine;
  const altPixelsPerMeter = worldSize / EARTH_CIRCUMFERENCE / latCosine;
  result.pixelsPerMeter = [altPixelsPerMeter, altPixelsPerMeter, altPixelsPerMeter];
  result.metersPerPixel = [1 / altPixelsPerMeter, 1 / altPixelsPerMeter, 1 / altPixelsPerMeter];
  result.pixelsPerDegree = [pixelsPerDegreeX, pixelsPerDegreeY, altPixelsPerMeter];
  result.degreesPerPixel = [1 / pixelsPerDegreeX, 1 / pixelsPerDegreeY, 1 / altPixelsPerMeter];
  if(highPrecision) {
    const latCosine2 = DEGREES_TO_RADIANS * Math.tan(latitude * DEGREES_TO_RADIANS) / latCosine;
    const pixelsPerDegreeY2 = pixelsPerDegreeX * latCosine2 / 2;
    const altPixelsPerDegree2 = worldSize / EARTH_CIRCUMFERENCE * latCosine2;
    const altPixelsPerMeter2 = altPixelsPerDegree2 / pixelsPerDegreeY * altPixelsPerMeter;
    result.pixelsPerDegree2 = [0, pixelsPerDegreeY2, altPixelsPerDegree2];
    result.pixelsPerMeter2 = [altPixelsPerMeter2, 0, altPixelsPerMeter2];
  }
  return result;
}

function getWorldPosition({
  longitude,
  latitude,
  zoom,
  scale,
  meterOffset,
  distanceScales=null
}) {
  scale = scale !== undefined? scale: zoomToScale(zoom);
  const center2d = lngLatToWorld([longitude, latitude], scale);
  const center = new Vector3(center2d[0], center2d[1], 0);
  if(meterOffset) {
    distanceScales = distanceScales || getDistanceScales({
      latitude,
      longitude,
      scale
    });
    const pixelPosition = new Vector3(meterOffset, meterOffset)
      .scale(distanceScales.pixelsPerMeter)
      .scale([1, -1, 1]);
    center.add(pixelPosition.vector);
  }
  return center;
}

function getViewMatrix({
  height,
  pitch,
  bearing,
  altitude,
  center=null,
  flipY=false
}) {
  const vm = createMat4();
  mat4_translate(vm, vm, [0, 0, -altitude]);
  mat4_scale(vm, vm, [1, 1, 1 / height]);
  mat4_rotateX(vm, vm, -pitch * DEGREES_TO_RADIANS);
  mat4_rotateZ(vm, vm, bearing * DEGREES_TO_RADIANS);
  if(flipY) mat4_scale(vm, vm, [1, -1, 1]);
  if(center) mat4_translate(vm, vm, center.negate());
  return vm;
}

function getProjectionParameters({
  width,
  height,
  altitude=DEFAULT_ALTITUDE,
  pitch=0,
  farZMultiplier=1
}) {
  const pitchRadians = pitch * DEGREES_TO_RADIANS;
  const halfFov = Math.atan(0.5 / altitude);
  const topHalfSurfaceDistance = Math.sin(halfFov) * altitude / Math.sin(Math.PI / 2 - pitchRadians - halfFov);
  const farZ = Math.cos(Math.PI / 2 - pitchRadians) * topHalfSurfaceDistance + altitude;
  return {
    fov: 2 * Math.atan((height / 2) / altitude),
    aspect: width / height,
    focalDistance: altitude,
    near: 0.1,
    far: farZ * farZMultiplier
  };
}

function getProjectionMatrix({
  width,
  height,
  pitch,
  altitude,
  farZMultiplier=10
}) {
  const {
    fov,
    aspect,
    near,
    far
  } = getProjectionParameters({
    width,
    height,
    altitude,
    pitch,
    farZMultiplier
  });
  const projectionMatrix = mat4_perspective(
    [],
    fov,
    aspect,
    near,
    far
  );
  return projectionMatrix;
}

function worldToPixels(xyz, pixelProjectionMatrix) {
  const [x, y, z = 0] = xyz;
  assert(Number.isFinite(x) && Number.isFinite(y) && Number.isFinite(z));
  return transformVector(pixelProjectionMatrix, [x, y, z, 1]);
}

function pixelsToWorld(xyz, pixelUnprojectionMatrix, targetZ = 0) {
  const [x, y, z] = xyz;
  assert(Number.isFinite(x) && Number.isFinite(y));
  if(Number.isFinite(z)) {
    const coord = transformVector(pixelUnprojectionMatrix, [x, y, z, 1]);
    return coord;
  }
  const coord0 = transformVector(pixelUnprojectionMatrix, [x, y, 0, 1]);
  const coord1 = transformVector(pixelUnprojectionMatrix, [x, y, 1, 1]);
  const z0 = coord0[2];
  const z1 = coord1[2];
  const t = z0 === z1 ? 0 : ((targetZ || 0) - z0) / (z1 - z0);
  return vec2_lerp([], coord0, coord1, t);
}

function deg2rad(deg) {
  return deg * (Math.PI / 180);
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  const R = 6371;
  const dLat = deg2rad(lat2 - lat1);
  const dLon = deg2rad(lon2 - lon1); 
  const a = 
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon / 2) * Math.sin(dLon / 2); 
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); 
  return R * c;
}

function getKmZoom(km) {
  const distances = [
    13316,
    6656,
    3328,
    1664,
    832,
    416,
    208,
    104,
    52,
    26,
    13,
    6.5,
    3.25,
    1.65
  ]
  for(let i = distances.length; i >= 0; i--)
    if(km < distances[i]) return i;
}

exports.equals = equals;
exports.createMat4 = createMat4;
exports.transformVector = transformVector;
exports.mod = mod;
exports.lerp = lerp;
exports.zoomToScale = zoomToScale;
exports.scaleToZoom = scaleToZoom;
exports.lngLatToWorld = lngLatToWorld;
exports.worldToLngLat = worldToLngLat;
exports.getMeterZoom = getMeterZoom;
exports.getDistanceScales = getDistanceScales;
exports.getWorldPosition = getWorldPosition;
exports.getViewMatrix = getViewMatrix;
exports.getProjectionParameters = getProjectionParameters;
exports.getProjectionMatrix = getProjectionMatrix;
exports.worldToPixels = worldToPixels;
exports.pixelsToWorld = pixelsToWorld;
exports.getDistanceFromLatLonInKm = getDistanceFromLatLonInKm;
exports.getKmZoom = getKmZoom;
