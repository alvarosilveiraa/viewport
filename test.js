const Viewport = require('./');

const viewport = new Viewport({
	width: 400,
    height: 400,
    latitude: -30.156002956073802,
    longitude: -51.16432550501543,
    km: 40
});

const values = [
	-30.156002956073802,
	-51.16432550501543,
	-30.156002956073802,
	-51.4329000501543
]

console.log(viewport.project([values[3], values[2]]));

